#!/python

"""
Module to control LED 4 Digit, 7 Segment display

<TODO: remove the physical GPIO handling and assignment into a new module>
"""

#import RPi.GPIO as GPIO
import time

# The pinouts are chosen based on the Kingbright part CC56-21SURKWA to give a
# 1:1 mapping
# GPIO Lines used for selection of the digit, the D1 is the Most Significant Digit
SELECT_D1 = 12
SELECT_D2 = 9
SELECT_D3 = 8
SELECT_D4 = 6
SELECT_LINES = [SELECT_D1,SELECT_D2,SELECT_D3,SELECT_D4]

# GPIO Lines used for setting of the segments
SEG_TOP_A = 11
SEG_TOP_RIGHT_B = 7
SEG_BOTTOM_RIGHT_C = 4
SEG_BOTTOM_D = 2
SEG_BOTTOM_LEFT_E = 1
SEG_TOP_LEFT_F = 10
SEG_MIDDLE_G = 5
SEG_POINT = 3
SEG_LINES = [SEG_TOP_A,SEG_TOP_RIGHT_B,SEG_BOTTOM_ROGHT_C,SEG_BOTTOM_D,SEG_BOTTOM_LEFT_E,SEG_TOP_LEFT_F,SEG_MIDDLE_G,SEG_POINT]

# constants for ON and OFF
SEG_ON = True
SEG_OFF = False

# initialise GPIO
for pin in SELECT_LINES:
    print("Initialising pin {}".format(pin))
    #GPIO.setup(pin, GPIO.OUT)
    #GPIO.output(pin, SEG_OFF)

for pin in SEG_LINES:
    print("Initialising pin {}".format(pin))
    #GPIO.setup(pin, GPIO.OUT)
    #GPIO.output(pin, SEG_OFF)

class SevenSegmentError(Exception):
    pass

class SevenSegment():
    def __init__(self, digits=4, init_value=0, colon_on=SEG_ON):
        self.number_of_digits = digits
        self.current_value = init_value
        self.set_value(self.current_value)
        self.set_colon(colon_on)


    def _get_digit_value_(self, digit):
        """
        Gets the value of the digit passed in as digit (base 1) where 1 is the
        Most Significant Digit.

        Returns a value 0 9 based on the current value of the digit number passed
        in as the parameter.
        """
        return int(self.current_value/10**(self.number_of_digits - digit - 1)) % 10

    def _write_digit_(self, digit):
        """
        Sets the GPIO correctly to select the appropriate digit (base 1) where
        1 is the Most Significant Digit.
        """
        print("Pulse on GPIO Line {}".format(SELECT_LINES[digit-1]))
        # GPIO.output(SELECT_LINES[digit-1],SEG_ON)
        time.sleep(0.001)
        # GPIO.output(SELECT_LINES[digit-1],SEG_OFF)

    def set_colon(self, value=SEG_OFF):
        """
        sets the colon points to either on or off based on the the value parameter
        """
        # the colon is the SEG_POINT lines on Digits 3 and 4
        self.colon = value
        D3_value = self._get_digit_value_(3)
        D4_value = self._get_digit_value_(4)

        # Set the IO for digit 3 and write.
        self._set_outputs_value_(D3_value)
        self._write_digit_(3)

        # Set the IO for digit 4 and write.
        self._set_outputs_value_(D4_value)
        self._write_digit_(4)

    def set_value(self, value):
        """
        Sets the current value

        Raises SevenSegmentError if the value is not appropriate for the size of the display
        """
        if value >= 10**self.number_of_digits || value < 0:
            raise SevenSegmentError(message="A value of {} can't go on a {} digit display".format(value,self.number_of_digits))

        for digit in range(self.number_of_digits):
            new_value = int(value/10**(digit))%10
            if new_value != self._get_digit_value_(self.number_of_digits-digit):
                self._set_outputs_value_(new_value)
                self._write_digit_(self.number_of_digits-digit)

        self.current_value = value

    def _set_outputs_value_(self, value):
        """
        Sets the outputs based on a value to give a 7 segment representation
        """
        if value in [0,2,3,5,6,7,8,9]:
            print("Set top segment")
            # GPIO.output(SEG_TOP_A,SEG_ON)
        else:
            print("Clear top segment")
            # GPIO.output(SEG_TOP_A,SEG_OFF)

        if value in [0,1,2,3,4,7,8,9]:
            print("Set top right segment")
            # GPIO.output(SEG_TOP_RIGHT_B,SEG_ON)
        else:
            print("Clear top right segment")
            # GPIO.output(SEG_TOP_RIGHT_B,SEG_OFF)

        if value in [0,1,3,4,5,6,7,8,9]:
            print("Set bottom right segment")
            # GPIO.output(SEG_BOTTOM_RIGHT_C,SEG_ON)
        else:
            print("Clear bottom right segment")
            # GPIO.output(SEG_BOTTOM_RIGHT_C,SEG_OFF)

        if value in [0,2,3,5,6,8]:
            print("Set bottom segment")
            # GPIO.output(SEG_BOTTOM_D,SEG_ON)
        else:
            print("Clear bottom segment")
            # GPIO.output(SEG_BOTTOM_D,SEG_OFF)

        if value in [0,2,6,8]:
            print("Set bottom left segment")
            # GPIO.output(SEG_BOTTOM_LEFT_E,SEG_ON)
        else:
            print("Clear bottom left segment")
            # GPIO.output(SEG_BOTTOM_LEFT_E,SEG_OFF)

        if value in [0,4,5,6,8,9]:
            print("Set top left segment")
            # GPIO.output(SEG_TOP_LEFT_F,SEG_ON)
        else:
            print("Clear top left segment")
            # GPIO.output(SEG_TOP_LEFT_F,SEG_OFF)

        if value in [2,3,4,5,6,8,9]:
            print("Set middle segment")
            # GPIO.output(SEG_MIDDLE_G,SEG_ON)
        else:
            print("Clear middle segment")
            # GPIO.output(SEG_MIDDLE_G,SEG_OFF)

        if self.colon:
             print("Set point segment")
             # GPIO.output(SEG_POINT,SEG_ON)
        else:
            print("Clear point segment")
            # GPIO.output(SEG_POINT,SEG_OFF)
